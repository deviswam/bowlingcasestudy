//
//  BowlingGame.swift
//  BowlingCaseStudy
//
//  Created by Waheed Malik on 26/06/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import Foundation

class BowlingGame {
    private var rolls = [Int](repeating: 0, count: 21)
    private var currentRoll = 0
    
    func frameNumber() -> Int {
        return (currentRoll / 2) + 1
    }
    
    func roll (pins : Int) -> [Int] {
        if currentRoll < rolls.count {
            rolls[currentRoll] = pins
            currentRoll += 1
        } else {
            currentRoll = 0
            rolls.removeAll()
        }
        
        var ballIndex = 0
        var completedFramesScore = [Int]()
        let endRange = currentRoll / 2
        for _ in 0..<endRange  {
            let frameSum = sumOfBallsInFrame(ballIndex)
            completedFramesScore.append(frameSum)
            ballIndex += 2
        }
        return completedFramesScore
    }
    
    func scoreSoFar () -> Int {
        var score = 0
        var ballIndex = 0
        for _ in 1...10  {
            if isStrike(ballIndex) { //strike
                score += 10 + strikeBonus(ballIndex)
                ballIndex += 1
            } else if isSpare(ballIndex) {
                score += 10 + spareBonus(ballIndex)
                ballIndex += 2
            } else {
                score += sumOfBallsInFrame(ballIndex)
                ballIndex += 2
            }
        }
        return score
    }
    
    func gameIsOver() -> Bool {
        if currentRoll == rolls.count {
            return true
        }
        return false
    }
    
    private func isStrike(_ ballIndex : Int) -> Bool {
        return rolls[ballIndex] == 10
    }
    
    private func isSpare(_ ballIndex : Int) -> Bool {
        return rolls[ballIndex] + rolls[ballIndex+1] == 10
    }
    
    private func strikeBonus(_ ballIndex: Int) -> Int {
        return rolls[ballIndex+1] + rolls[ballIndex+2]
    }
    
    private func spareBonus(_ ballIndex: Int) -> Int {
        return rolls[ballIndex+2]
    }
    
    private func sumOfBallsInFrame(_ ballIndex: Int) -> Int {
        return rolls[ballIndex] + rolls[ballIndex+1]
    }
}

