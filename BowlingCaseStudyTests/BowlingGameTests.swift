//
//  BowlingGame.swift
//  BowlingCaseStudyTests
//
//  Created by Waheed Malik on 26/06/2018.
//  Copyright © 2018 Waheed Malik. All rights reserved.
//

import XCTest
@testable import BowlingCaseStudy

class BowlingGameTests: XCTestCase {
    
    var game: BowlingGame!
    override func setUp() {
        super.setUp()
        game = BowlingGame()
    }
    
    override func tearDown() {
        game = nil
        super.tearDown()
    }
    
    func testScoreSoFar_withGutterGame_returnsZero() {
        // When
        rollMany(pins: 0, times: 20)
        
        // Then
        XCTAssertEqual(game.scoreSoFar(), 0)
    }
    
    func testScoreSoFar_withAllOnes_returns20() {
        // When
        rollMany(pins: 1, times: 20)
        
        // Then
        XCTAssertEqual(game.scoreSoFar(), 20)
    }
    
    func testOneSpare() {
        // When
        rollSpare()
        _ = game.roll(pins: 3)
        rollMany(pins: 0, times: 17)
        XCTAssertEqual(game.scoreSoFar(), 16)
    }
    
    func testOneStrike() {
        // When
        _ = game.roll(pins: 10)
        _ = game.roll(pins: 3)
        _ = game.roll(pins: 4)
        rollMany(pins: 0, times: 17)
        XCTAssertEqual(game.scoreSoFar(), 24)
    }
    
    func testFrameNumber_withoutARoll_shouldReturnOne() {
        // When
        let frameNumber = game.frameNumber()
        // Then
        XCTAssertEqual(frameNumber, 1)
    }
    
    func testFrameNumber_afterASingleRoll_shouldReturnOne() {
        // When
        _ = game.roll(pins: 0)
        let frameNumber = game.frameNumber()
        // Then
        XCTAssertEqual(frameNumber, 1)
    }
    
    func testFrameNumber_afterTwoRolls_shouldReturnTwo() {
        // When
        rollMany(pins: 0, times: 2)
        let frameNumber = game.frameNumber()
        // Then
        XCTAssertEqual(frameNumber, 2)
    }
    
    func testFrameNumber_withFourRolls_shouldReturnThree() {
        // When
        rollMany(pins: 0, times: 4)
        let frameNumber = game.frameNumber()
        // Then
        XCTAssertEqual(frameNumber, 3)
    }
    
    func testRoll_afterOneRoll_shouldReturnArrayOfZeroLength() {
        // When
        let frameScores = game.roll(pins: 2)
        // Then
        XCTAssertEqual(frameScores.count, 0)
    }
    
    func testRoll_afterCompletingFirstFrame_shouldReturnArrayOfLengthOneWithCorrectFrameScore() {
        // When
        _ = game.roll(pins: 2)
        let frameScores = game.roll(pins: 2)
        // Then
        XCTAssertEqual(frameScores.count, 1)
        XCTAssertEqual(frameScores[0], 4)
    }
    
    func testRoll_afterCompletingFirstFrameAndARoll_shouldReturnArrayOfLengthOneWithCorrectFrameScore() {
        // When
        rollMany(pins: 2, times: 2)
        let frameScores = game.roll(pins: 2)
        // Then
        XCTAssertEqual(frameScores.count, 1)
        XCTAssertEqual(frameScores[0], 4)
    }
    
    func testRoll_afterCompletingTwoFrames_shouldReturnArrayOfLengthTwoWithCorrectFrameScores() {
        // When
        rollMany(pins: 2, times: 2)
        _ = game.roll(pins: 8)
        let frameScores = game.roll(pins: 2)
        // Then
        XCTAssertEqual(frameScores.count, 2)
        XCTAssertEqual(frameScores[0], 4)
        XCTAssertEqual(frameScores[1], 10)
    }
    
    func testGameIsOver_whenPlayerIsOnFirstFrame_shouldReturnFalse() {
        // When
        _ = game.roll(pins: 2)
        // Then
        XCTAssertFalse(game.gameIsOver())
    }
    
    func testGameIsOver_whenTenthFrameIsScored_shouldReturnTrue() {
        // When
        rollMany(pins: 0, times: 21)
        // Then
        XCTAssertTrue(game.gameIsOver())
    }
    
    func testGameIsOver_whenTenthFrameIsScored_andThenNextRollIsPlayed_shouldStartNewGame() {
        // When
        rollMany(pins: 0, times: 21)
        // Then
        XCTAssertTrue(game.gameIsOver())

        /////////// NEXT ROLL IS PLAYED WHICH SHOULD TRIGGER NEW GAME //////////
        // When
        _ = game.roll(pins: 2)
        // Then
        XCTAssertEqual(game.frameNumber(), 1)
    }
    
    
    
    // MARK: PRIVATE METHODS
    private func rollMany(pins: Int, times: Int) {
        for _ in 1...times {
            _ = game.roll(pins: pins)
        }
    }
    
    private func rollSpare() {
        _ = game.roll(pins: 5)
        _ = game.roll(pins: 5)
    }
}
